package ehrsynergy;

import ehrsynergy.CustomElement.CustomElement;
import ehrsynergy.CustomElement.Tags;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class SubElement {

    private static By SUB_ELEMENT = By.className("_2yKSv");

    private int getSize(List<WebElement> subElements) {
        if (subElements == null) {
            return  1;
        }
        else {
            return subElements.size();
        }
    }

    public StringBuilder getSubElements(int countSubElements) {
        StringBuilder subElements = new StringBuilder();

        for (int i = 1; i <= countSubElements; i++) {
            subElements.append(" ?? ");
        }

        return subElements;
    }

    private void addElements(final WebDriver driver, List<WebElement> subElements) throws InterruptedException {
        int subElementsSize = getSize(subElements);

        WebDriverWait wait = new WebDriverWait(driver, 10);

        for (int j=0; j < subElementsSize; j++) {

            if (subElementsSize > 1) {
                subElements.get(j).click();
            }

            WebElement inputFieldPopUp;
            wait.withTimeout(100, TimeUnit.MILLISECONDS);
            try {
                inputFieldPopUp = wait.until(ExpectedConditions.visibilityOfElementLocated(Encounter.SUB_ELEMENT_POPUP_INPUT_FIELD));
            }
            catch (TimeoutException exception) {
                subElements.get(j).click();
                inputFieldPopUp = wait.until(ExpectedConditions.visibilityOfElementLocated(Encounter.SUB_ELEMENT_POPUP_INPUT_FIELD));
            }

            inputFieldPopUp.sendKeys(Tags.getTag(), Keys.ENTER, Keys.ENTER);
//            ((JavascriptExecutor)driver).executeScript("arguments[0].value = arguments[1];", inputFieldPopUp, Tags.getTag());
//            inputFieldPopUp.sendKeys(Keys.RETURN);

            wait.withTimeout(200, TimeUnit.MILLISECONDS);
            try {
                wait.until(ExpectedConditions.invisibilityOfElementLocated(Encounter.SUB_ELEMENT_POPUP_INPUT_FIELD));
            }
            catch (TimeoutException exception) {
                WebElement input = wait.until(ExpectedConditions.visibilityOfElementLocated(Encounter.SUB_ELEMENT_POPUP_INPUT_FIELD));
                input.clear();
                input.sendKeys(Tags.getTag(), Keys.ENTER, Keys.ENTER);
            }

            TimeUnit.MILLISECONDS.sleep(400);
        }
    }

    public void attachSubElements(WebDriver driver, WebElement addedSubElement) throws InterruptedException {
        this.addElements(driver, null);

        WebElement addedCustomElement = Utils.waitWebElement(driver, CustomElement.CUSTOM_ELEMENT, addedSubElement);
        List<WebElement> subElements = addedCustomElement.findElements(SubElement.SUB_ELEMENT);

        if (subElements.size() != 0) {
            subElements.remove(0);
            this.addElements(driver, subElements);
            driver.navigate().back();
        }

    }
}
