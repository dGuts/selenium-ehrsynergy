package ehrsynergy;

import ehrsynergy.CustomElement.CustomElement;
import ehrsynergy.CustomElement.CustomElementFactory;
import ehrsynergy.CustomElement.RandomString;
import ehrsynergy.CustomElement.Tags;
import ehrsynergy.Driver.DriverFactory;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.HasInputDevices;
import org.openqa.selenium.interactions.Keyboard;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Application {

  public static void main(String[] args) throws InterruptedException {
    DriverFactory driverFactory = new DriverFactory();
    final WebDriver driver = driverFactory.getDriver();

    Login login = new Login();
    login.login(driver, "https://ehrsynergy.com/signin");
//        login.login(driver, "https://ehryou.dev.gomel.ximxim.com/");

    Utils.waitWebElement(driver, By.className("_3xJTW"));

    driver.get("https://ehrsynergy.com/charts/900/encounters");
//        driver.get("https://ehryou.dev.gomel.ximxim.com/app/charts/681/encounters");

    CustomElementFactory customElementFactory = new CustomElementFactory();
    CustomElement customElement = customElementFactory.getCustomElement(true, true, 3);
    final List<String> customs = customElement.fromFile();

    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    Date date = new Date();

    for (int count=1; count <=10; count++) {

      WebElement startNewNote = Utils.waitWebElement(driver, Encounter.START_NEW_NOTE);
      startNewNote.click();

      WebElement systemsContainer = Utils.waitWebElement(driver, By.className("_3TbzX"));
      List<WebElement> systems = systemsContainer.findElements(By.className("_2Z3xB"));
      WebDriverWait wait = new WebDriverWait(driver, 10);
      for (int z=0; z < systems.size(); z++) {

        if (z == 2) {
          continue;
        }

        if (z > 0) {
          systems.get(z).click();
        }

        Encounter encounter = new Encounter();
        final List<WebElement> addedSystems;
        if (z != 5) {
          addedSystems  = encounter.getAddedContainers(driver);
        }
        else {
          addedSystems = driver.findElements(Encounter.ADDED_SYSTEM);
        }

        RandomString randomString = new RandomString();
//                CustomElementFactory customElementFactory = new CustomElementFactory();
//                CustomElement customElement = customElementFactory.getCustomElement(true, true, 3);

        for (int i=0; i < addedSystems.size(); i++) {

          WebElement inputField = driver.findElements(Encounter.ADDED_SYSTEM).get(i).findElement(Encounter.INPUT_FIELD);

          if (z == 6 && i == 0) {
            inputField.sendKeys(randomString.createCustomString(), Keys.RETURN);
          }
          else if (z == 5 && i == 0 || z == 7) {
            inputField.click();
            ExpectedCondition<List<WebElement>> condition = ExpectedConditions.visibilityOfAllElementsLocatedBy(By.className("_3z7mL "));
            List<WebElement> elements = wait.until(condition);
            int countToAdd = (int)(1 + Math.random()*(elements.size()-2));
            elements.get(countToAdd).click();

            systems.get(z).click();

            if (z == 5 && i == 0) {
              wait.withTimeout(200, TimeUnit.MILLISECONDS);
              try {
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("UBP9k"))).findElements(By.className("_2F3g4")).get(1).click();
                systems.get(z).click();
                TimeUnit.MILLISECONDS.sleep(700);
              }
              catch (TimeoutException exception) {
                systems.get(z).click();
                TimeUnit.MILLISECONDS.sleep(700);
              }
            }
          }
          else {

            inputField = Utils.waitWebElement(driver, Encounter.INPUT_FIELD, driver.findElements(Encounter.ADDED_SYSTEM).get(i));
            for (final String element : customs) {

              String data = element + " " + dateFormat.format(date);
              ((JavascriptExecutor)driver).executeScript("arguments[0].value = arguments[1];", inputField, data);
              inputField.sendKeys(Keys.ENTER, Keys.ENTER);

              Thread.sleep(1000);

              if (element.contains("??")) {
                customElement.attachCustomElement(data, i, driver, addedSystems.get(i));
              }

            }
            driver.navigate().back();

          }
        }
      }

      System.out.println(count);

      driver.findElements(Encounter.ENCOUNTER_TAB).get(1).click();
    }

    driver.quit();
  }
}
