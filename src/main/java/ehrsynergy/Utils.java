package ehrsynergy;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Utils {

    public static WebElement waitWebElement(WebDriver driver, final By criteria) {
         return  (new WebDriverWait(driver, 40)).until(new ExpectedCondition<WebElement>() {
            public WebElement apply(WebDriver webDriver) {
                return webDriver.findElement(criteria);
            }
        });
    }

    public static WebElement waitWebElement(WebDriver driver, final By criteria, final WebElement element) {
        return  (new WebDriverWait(driver, 10)).until(new ExpectedCondition<WebElement>() {
            public WebElement apply(WebDriver webDriver) {
                return element.findElement(criteria);
            }
        });
    }
}
