package ehrsynergy;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Login {

    public static final By LOGIN_FORM = By.className("_35Gev");
    public static final By EMAIL_INPUT = By.name("email");
    public static final By PASSWORD_INPUT = By.name("password");
    public static final By LOGIN_SUBMIT = By.className("_28Lgs");

    public void login(WebDriver driver, String url) {
        driver.get(url);
        WebElement loginForm = Utils.waitWebElement(driver, Login.LOGIN_FORM);

        loginForm.findElement(Login.EMAIL_INPUT).sendKeys("zhiko32@gmail.com");
        loginForm.findElement(Login.PASSWORD_INPUT).sendKeys("123456");
        loginForm.findElement(Login.LOGIN_SUBMIT).submit();
    }
}
