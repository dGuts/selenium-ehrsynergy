package ehrsynergy.Driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class DriverFactory {

    public WebDriver getDriver() {
        System.setProperty("webdriver.chrome.driver", "C:\\D\\projects\\selenium-ehrsynergy\\src\\main\\java\\ehrsynergy\\Driver\\chromedriver.exe");
        return new ChromeDriver();
    }
}
