package ehrsynergy.CustomElement;

import ehrsynergy.StringUtils;

public class RandomString {

    private final String ABC = "qwertyuiopasdfghjklzxcvbnm";

    public String createCustomString() {
        int lengthCustomString = (int)(1+ Math.random()*(this.ABC.length()));

        StringBuilder customString = new StringBuilder();

        for (int i=0; i < lengthCustomString; i++) {
            customString.append(this.ABC.charAt((int)(1+ Math.random()*(this.ABC.length()-1))));
        }

        return StringUtils.isNeedStrong(customString.toString());
    }

}
