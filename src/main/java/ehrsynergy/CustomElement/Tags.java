package ehrsynergy.CustomElement;

import ehrsynergy.StringUtils;

public class Tags {

    public static String getTag() {
        TagsEnum tags[] = TagsEnum.values();
        return StringUtils.isNeedStrong(tags[(int)(Math.random()*(tags.length))].getTag());
    }

}
