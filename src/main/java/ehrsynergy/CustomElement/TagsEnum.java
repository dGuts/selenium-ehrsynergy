package ehrsynergy.CustomElement;

public enum TagsEnum {

    PATIENT("<<patient>>"),
    DOB("<<dob>>"),
    GENDER("<<gender>>"),
    SHE_HE("<<he/she>>"),
    AGE("<<age>>"),
    HIS_HER("<<his/her>>");

    public String tag;

    TagsEnum(String tag) {
        this.tag = tag;
    }

    public String getTag() {
        return this.tag;
    }
}
