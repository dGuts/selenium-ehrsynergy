package ehrsynergy.CustomElement;

import ehrsynergy.SubElement;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CustomElementFactory {

    private final RandomString randomString;
    private final SubElement subElement;

    public CustomElementFactory() {
        this.subElement = new SubElement();
        this.randomString = new RandomString();
    }

    public CustomElement getCustomElement(boolean isNeedTag, boolean isNeedString, int countSubElements) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

        StringBuilder customElement = new StringBuilder();

        if (isNeedTag) {
            customElement.append(Tags.getTag());
        }
        if (isNeedString) {
            customElement.append(this.randomString.createCustomString());
        }

        String finalCustomString = String.valueOf(customElement) +
                subElement.getSubElements(countSubElements) +
                dateFormat.format(date);
        //        customElement.toString() + " " + subElement.getSubElements(getCountSubElements()) + " " + dateFormat.format(date);

        return new CustomElement(finalCustomString, countSubElements);
    }

}
