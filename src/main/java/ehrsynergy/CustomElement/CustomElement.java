package ehrsynergy.CustomElement;

import ehrsynergy.Encounter;
import ehrsynergy.SubElement;
import ehrsynergy.Utils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class CustomElement {

    public static final By CUSTOM_ELEMENT = By.className("_1WIlJ");

    private int countSubElements;
    private final SubElement subElement;

    private int getCountSubElements() {
        return countSubElements;
    }

    public String getCustomElement() {
        return customElement;
    }

    private String customElement;

    CustomElement(String customElement, int countSubElements) {
        this.customElement = customElement;
        this.countSubElements = countSubElements;

        this.subElement = new SubElement();
    }


    private void waitPopUpHide(WebDriverWait wait) {
        wait.withTimeout(200, TimeUnit.MILLISECONDS);
        try {
            wait.until(ExpectedConditions.invisibilityOfElementLocated(Encounter.SUB_ELEMENT_POPUP_INPUT_FIELD));
        }
        catch (TimeoutException exception) {
            WebElement input = wait.until(ExpectedConditions.visibilityOfElementLocated(Encounter.SUB_ELEMENT_POPUP_INPUT_FIELD));
            input.clear();
            input.sendKeys(Tags.getTag(), Keys.ENTER, Keys.ENTER);
        }
    }

    public WebElement waitUntil(WebDriver driver, WebElement toElement, String data) throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 3);

        wait.withTimeout(100, TimeUnit.MILLISECONDS);
        WebElement element = null;
        while (element == null) {
            Thread.sleep(1000);
            WebElement inputField = toElement.findElement(Encounter.INPUT_FIELD);

            try {
                element = wait.until(ExpectedConditions.visibilityOfElementLocated(Encounter.SUB_ELEMENT_POPUP_INPUT_FIELD));
            }
            catch (TimeoutException exception) {
                ((JavascriptExecutor)driver).executeScript("arguments[0].value = arguments[1];", inputField, data);
                inputField.sendKeys(Keys.ENTER);
            }
        }

        return element;
    }

    public void attachCustomElement(String data, int systemItem, WebDriver driver, WebElement toElement) throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 3);

//        WebElement inputFieldPopUp = waitUntil(driver, toElement, data);
        WebElement inputFieldPopUp = null;
        try {
            inputFieldPopUp = wait.until(ExpectedConditions.visibilityOfElementLocated(Encounter.SUB_ELEMENT_POPUP_INPUT_FIELD));
        }
        catch (TimeoutException exception) {
            WebElement input = driver.findElements(Encounter.ADDED_SYSTEM).get(systemItem).findElement(Encounter.INPUT_FIELD);
            input.clear();
            ((JavascriptExecutor)driver).executeScript("arguments[0].value = arguments[1];", input, data);
            Thread.sleep(1000);
            input.sendKeys(" ", Keys.RETURN);
            inputFieldPopUp = wait.until(ExpectedConditions.visibilityOfElementLocated(Encounter.SUB_ELEMENT_POPUP_INPUT_FIELD));
        }

        if (inputFieldPopUp != null) {

            try {
                inputFieldPopUp.sendKeys(Tags.getTag());
                inputFieldPopUp.sendKeys(Keys.RETURN, Keys.RETURN);

                waitPopUpHide(wait);

                Thread.sleep(1000);
            }
            catch (TimeoutException exception) {
                driver.findElements(Encounter.ADDED_SYSTEM).get(systemItem).findElement(Encounter.INPUT_FIELD).sendKeys(Keys.RETURN);
            }

            WebDriverWait waiting = new WebDriverWait(driver, 15, 100);
            ExpectedCondition<List<WebElement>> condition = ExpectedConditions.visibilityOfAllElementsLocatedBy(Encounter.ADDED_CUSTOM_ELEMENT);
            List<WebElement> allCustomsElement = waiting.until(condition);
            List<WebElement> allSubElements = allCustomsElement.get(allCustomsElement.size() - 1).findElements(Encounter.ADDED_SUB_ELEMENT);

            if (allSubElements.size() > 1) {
                allSubElements.remove(0);

                for (int j = 0; j < allSubElements.size(); j++) {

                    ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);", allCustomsElement.get(allCustomsElement.size() - 1));
                    ((JavascriptExecutor)driver).executeScript("arguments[0].click();", allSubElements.get(j));
                    WebElement inputField = driver.findElement(Encounter.INPUT_FIELD);
                    ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);", inputField);

                    try {
                        inputFieldPopUp = wait.until(ExpectedConditions.visibilityOfElementLocated(Encounter.SUB_ELEMENT_POPUP_INPUT_FIELD));
                    }
                    catch (TimeoutException exception) {
                        ((JavascriptExecutor)driver).executeScript("arguments[0].click();", allSubElements.get(j));
                        ((JavascriptExecutor)driver).executeScript("arguments[0].click();", allSubElements.get(j));
                        inputFieldPopUp = wait.until(ExpectedConditions.visibilityOfElementLocated(Encounter.SUB_ELEMENT_POPUP_INPUT_FIELD));
                    }

                    inputFieldPopUp.sendKeys(Tags.getTag());
                    inputFieldPopUp.sendKeys(Keys.RETURN);

                    waitPopUpHide(wait);
//                    wait.withTimeout(200, TimeUnit.MILLISECONDS);
//                    try {
//                        wait.until(ExpectedConditions.invisibilityOfElementLocated(Encounter.SUB_ELEMENT_POPUP_INPUT_FIELD));
//                    }
//                    catch (TimeoutException exception) {
//                        WebElement input = wait.until(ExpectedConditions.visibilityOfElementLocated(Encounter.SUB_ELEMENT_POPUP_INPUT_FIELD));
//                        input.clear();
//                        input.sendKeys(Tags.getTag(), Keys.ENTER, Keys.ENTER);
//                    }

                    TimeUnit.MILLISECONDS.sleep(400);
                }

            }

//        if (getCountSubElements() != 0) {
//            subElement.attachSubElements(driver, toElement);
//        }
        }
    }

    public List<String> fromFile() {
        List<String> customElements = new ArrayList<>();

        try (FileInputStream fileInputStream = new FileInputStream("src/main/java/ehrsynergy/data");
             BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream))) {

            String s;
            StringBuilder line = new StringBuilder();
            while ((s = bufferedReader.readLine()) != null) {
                if (s.length() != 0) {
                    line.append(s);
                }
                else {
                    customElements.add(line.toString());
                    line.setLength(0);
                }
            }
            customElements.add(line.toString());
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return customElements;
    }
}
