package ehrsynergy;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class Encounter {

    public static final By SYSTEMS = By.className("_2Z3xB");
    public static final By START_NEW_NOTE = By.className("_2mcY3");
    public static final By ENCOUNTER_TAB = By.className("_2L0Wl");

    public static final By AVAILABLE_SYSTEMS = By.className("_3z7mL");
    public static final By ADDED_SYSTEM = By.className("_3rMav");
    public static final By INPUT_FIELD = By.className("_2IgV6");

    public static final By SUB_ELEMENT_POPUP = By.className("jKrCW");
    public static final By SUB_ELEMENT_POPUP_INPUT_FIELD = By.xpath("//input[@class='_2syIr']");

    public static final By ADDED_CUSTOM_ELEMENT = By.cssSelector("._3_--K");
    public static final By ADDED_SUB_ELEMENT = By.className("_2yKSv");

    public List<WebElement> getAddedContainers(WebDriver driver) throws InterruptedException {
        Utils.waitWebElement(driver, Encounter.AVAILABLE_SYSTEMS);

        final List<WebElement> availableSystems = driver.findElements(Encounter.AVAILABLE_SYSTEMS);

        for (WebElement availableSystem : availableSystems) {
            if (availableSystem.isDisplayed()) {
                availableSystem.click();
                TimeUnit.MILLISECONDS.sleep(300);
            }
        }

        TimeUnit.MILLISECONDS.sleep(500);

        final List<WebElement> addedSystems = driver.findElements(Encounter.ADDED_SYSTEM);

        try {
            (new WebDriverWait(driver, 1)).until((ExpectedCondition<Boolean>) webDriver -> addedSystems.size() == availableSystems.size());
        }
        catch (TimeoutException exception) {
            final List<WebElement> availableSystems2 = driver.findElements(Encounter.AVAILABLE_SYSTEMS);

            for (WebElement available : availableSystems2) {
                if (available.isDisplayed()) {
                    available.click();
                    TimeUnit.MILLISECONDS.sleep(300);
                }
            }

            (new WebDriverWait(driver, 1)).until((ExpectedCondition<Boolean>) webDriver -> addedSystems.size() == availableSystems.size());
        }

        return addedSystems;
    }
}
